#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include "tcp.h"
#define TCB_BUF_SIZE 1000

typedef char u8_t;
typedef unsigned short int u16_t;
typedef unsigned int u32_t;
typedef bool state_t;

static u8_t tcb_header_len = 0x05;

typedef enum{
    FLAG_ACK = 0x40, FLAG_SYN = 0x80, FLAG_FIN = 0x04
}flag;

typedef struct tcb_h{
    u16_t tcb_our_port;
    u16_t tcb_their_port;
    u32_t tcb_our_sequence_num;
    u32_t tcb_our_expected_ack;
    u8_t tcb_header_len;
    u8_t tcb_flags;
    u16_t tcb_win_size;
    u16_t tcb_checksum;
}tcb_ht;

typedef struct tcb_d{
    u32_t tcb_their_sequence_num;
    ipaddr_t tcb_our_ip_addr;
    ipaddr_t tcb_their_ip_addr;
    char tcb_data[TCB_BUF_SIZE];
    char *tcb_p_data;
    int tcb_data_left;
    state_t tcb_state;
}tcb_dt;

static tcb_ht g_tcb;
static tcb_dt t_tcb;

int tcp_socket(){

    bzero ((char*)&g_tcb, sizeof (g_tcb));
    bzero ((char*)&t_tcb, sizeof (t_tcb));
    t_tcb.tcb_state = false;
    return 0;
}

int tcp_listen(int port, ipaddr_t *src){
    t_tcb.tcb_our_ip_addr = *src;
    g_tcb.tcb_our_port = port;
    t_tcb.tcb_state = true;
    g_tcb.tcb_our_sequence_num = 1000;
    g_tcb.tcb_our_expected_ack = 1;
    t_tcb.tcb_their_sequence_num = 0;
    return 0;
}

int tcp_connect(ipaddr_t dst, int port){
    t_tcb.tcb_their_ip_addr = dst;
    g_tcb.tcb_their_port = port;
    t_tcb.tcb_state = true;
    g_tcb.tcb_our_sequence_num = 0;
    g_tcb.tcb_our_expected_ack = 1000;
    t_tcb.tcb_their_sequence_num = 1000;
    return 0;   
}

int tcp_read(char *buf, int maxlen){
    int len = 0, max_allowed_size; 
    len = handle_packet();
    
    if (len > 1){
        printf("Length of the recieved packet %d", len);
        max_allowed_size = min(maxlen, len);
        printf(t_tcb.tcb_data);
            
        return max_allowed_size;
    }
    if(len == -1)
        return -1;
}

int tcp_write(char *buf, int len){
    int packet_size, ack;
    strcpy(t_tcb.tcb_data, buf);
    t_tcb.tcb_data_left = len;
    while(t_tcb.tcb_data_left>0){
    packet_size = min(t_tcb.tcb_data_left,TCB_BUF_SIZE);
    do{
        send_tcp_packet(t_tcb.tcb_their_ip_addr, g_tcb.tcb_our_port, g_tcb.tcb_their_port, g_tcb.tcb_our_sequence_num, g_tcb.tcb_our_expected_ack_num, g_tcb.tcb_flags, g_tcb.tcb_win_size, t_tcb.tcb_data, packet_size);

        ack = wait_for_packet();
    }while(ack != g_tcb.tcb_our_expected_ack);
    g_tcb.tcb_our_expected_ack += 1;
    t_tcb.tcb_data_left-=packet_size;
    }
    return packet_size;
}

int close(){
}

int send_tcp_packet(ipaddr_t dst, u16_t src_port, u16_t dst_port, u32_t seq_nb, u32_t ack_nb, u8_t flags, u16_t win_sz, const char *data, int data_sz){
    static int id = 0;
    int res, total_size;
    char *msg;
    struct tcp_h *my_send_packet;
    total_size = data_sz + tcb_header_len;
    msg = malloc(total_size);
    my_send_packet = (tcp_h *)msg;

    my_send_packet->tcb_our_port = src_port;
    my_send_packet->tcb_their_port = dst_port;
    my_send_packet->tcb_our_sequence_num = seq_nb;
    my_send_packet->tcb_our_expected_ack = ack_nb;
    //my_send_packet->tcb_header_len = sizeof(g_tcb);
    my_send_packet->tcb_flags = flag;
    my_send_packet->tcb_win_size = TCB_BUF_SIZE;
    my_send_packet->tcb_checksum = 0xFFFF;
    //copy the data to the remaining memory
    
    res = ip_send(dst, IPPROTO_TCP, ++id , msg , total_size);
    return res;
}

int recv_tcp_packet(ipaddr_t *src, u16_t *src_port, u16_t *dst_port, u32_t *seq_nb, u32_t *ack_nb, u8_t *flags, u16_t *win_sz, const char *data, int *data_sz){
 
    static int id;
    int len, header_len, data_len;

    struct tcp_h *my_recv_packet;
    char *msg;
    len = ip_receive(&src, &dst, &proto, &id, &msg); 
    data_len = len - tcb_header_len;
    my_recv_packet = (tcp_h *)msg;
    
    //copy data from the remaining memory
    return data_len;
}

static int handle_packet(){
    int len;
    while(len = recv_tcp_packet(t_tcb.tcb_their_ip_addr, &g_tcb.tcb_their_port, &g_tcb.tcb_our_port, &t_tcb.tcb_their_sequence_num, &g_tcb.tcb_our_expected_ack, &g_tcb.tcb_flags, &g_tcb.tcb_win_size, &t_tcb.tcb_data)){
        if(len!=0){
            return len;
        }
        else if(g_tcb.tcb_flags == FLAG_FIN){
            tcp_close();
            return 0;
        }
        else if(g_tcb.tcb_flags == FLAG_ACK){
            if(t_tcb.tcb_their_sequence_num == g_tcb.our_expected_ack){
                g_tcb.our_expected_ack += 1;
                //printf("Ack recieved: %d", t_tcb_their_sequence_num);
                return 0;
            }
            else return -1;
        }
        else return -1;
    }
}

static int wait_for_packet(){
    int len;
    len = recv_tcp_packet(&t_tcb.tcb_their_ip_addr, &t_tcb.tcb_their_port, &g_tcb.tcb_our_port, &t_tcb.tcb_their_sequence_num, &g_tcb.tcb_our_expected_ack, &g_tcb.tcb_flags, &g_tcb.tcb_win_size, &t_tcb.tcb_data);

    if(g_tcb.tcb_flags == FLAG_ACK){
        if(t_tcb.tcb_their_sequence_num == t_tcb.our_expected_ack){
            t_tcb.our_expected_ack += 1;
            printf("Ack recieved: %d", t_tcb_their_sequence_num);
            return t_tcb.tcb_their_sequence_num;
            }
            else return -1;
    }
}

int min (int a, int b){
    if(a<b)
        return a;
    return b;
}

