#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "ip.h"
#include "tcp.h"
#define TCB_BUF_SIZE 1000 

//typedef char u8_t;
typedef unsigned short int u16_t;
typedef unsigned int u32_t;
typedef bool state_t;
typedef unsigned long ipaddr_t;
static u8_t tcb_header_len = 0x05;

typedef enum{
    FLAG_ACK = 0x40, FLAG_SYN = 0x80, FLAG_FIN = 0x04
}flag;

typedef struct tcb_h{
    u16_t tcb_our_port;
    u16_t tcb_their_port;
    u32_t tcb_our_sequence_num;
    u32_t tcb_our_expected_ack;
    u8_t tcb_header_len;
    u8_t tcb_flags;
    u16_t tcb_win_size;
    u16_t tcb_checksum;
}tcb_ht;

typedef struct tcb_d{
    u32_t tcb_their_sequence_num;
    ipaddr_t tcb_our_ip_addr;
    ipaddr_t tcp_their_ip_addr;
    char tcb_data[TCB_BUF_SIZE];
    char *tcb_p_data;
    int tcb_data_left;
    state_t tcb_state;
}tcb_dt;

static tcb_ht g_tcb;
static tcb_dt t_tcb;

int send_tcp_packet(ipaddr_t dst, u16_t src_port, u16_t dst_port, u32_t seq_nb, u32_t ack_nb, u8_t flags, u16_t win_sz, const char *data, int data_sz){
    static int id = 0;
    int res = 0, total_size;

    //tcb_ht *my_send_packet;
    total_size = data_sz + tcb_header_len;
    /*my_send_packet = malloc(total_size + 1);

    (*my_send_packet).tcb_our_port = htons(2345); //src_port;
    (*my_send_packet).tcb_their_port = htons(2345); //dst_port;
    (*my_send_packet).tcb_our_sequence_num = 1; //seq_nb;
    (*my_send_packet).tcb_our_expected_ack = 1; //ack_nb;
    //my_send_packet).tcb_header_len = sizeof(g_tcb);
    (*my_send_packet).tcb_flags = flags;
    (*my_send_packet).tcb_win_size = TCB_BUF_SIZE;
    (*my_send_packet).tcb_checksum = 0xFFFF;*/
    //copy the data to the remaining memory
    printf("YO_s");
    res = ip_send(dst, IPPROTO_TCP, ++id, "mess", total_size);
    printf(" data %d", res);
    return res;
    //return -1;
}

int recv_tcp_packet(ipaddr_t *src, u16_t *src_port, u16_t *dst_port, u32_t *seq_nb, u32_t *ack_nb, u8_t *flags, u16_t *win_sz, const char *data, int *data_sz){
 
    static unsigned short int id;
    unsigned short int proto;
    int len, header_len, data_len;

    //struct tcp_h *my_recv_packet;
    char *msg;
    ipaddr_t dst,srcn;
    unsigned short prototype;
    printf("YO_R");
    len = ip_receive(&srcn, &dst, &prototype, &id, &msg); 
    data_len = len - tcb_header_len;
    //my_recv_packet = msg;
    printf("Data Lenght %d", data_len);
    printf("Message %c",*msg);
    //copy data from the remaining memory
    //return data_len;
    return -1;
}
