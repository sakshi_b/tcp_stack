#include<stdio.h>
#include<netinet/in.h>
#include"tcp.h"

int main () {
	int len;
	char *msg = "hello";
	ipaddr_t dst;
	u16_t dst_p;
	dst_p = htons( 2345 );
	inet_aton( "127.0.0.1", ( struct in_addr * ) &dst );

	tcp_socket();
	tcp_connect( dst, dst_p );

    	if ( ( len = tcp_write( msg, 6 ) ) > 0 ) {
		printf("%d write to socket\n", len);
	}
	tcp_close();
	return 0;
}

