/*
 * socket_test.c
 *
 *  Created on: 22-Apr-2011
 *      Author: Rahul Krishnan
 */

#include <stdio.h>
#include"tcp.h"
#include<netinet/in.h>

#define OUR_PORT 4444

#define OUR_IP "192.168.0.1"
#define THEIR_IP "192.168.0.2"

int
main () {
	ipaddr_t *src;
	u16_t our_port;
	our_port = htons(OUR_PORT);

	tcp_socket();
	tcp_listen(our_port, src);
	tcp_close();
	return 0;
}


