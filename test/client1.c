#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include "ip.h"
#include "tcp.h"

typedef unsigned long ipaddr_t;

int main()
{
	int sockfd;
	struct sockaddr_in serv_addr;
    struct sockaddr_in sa;
    char data[100] = {0};
    ipaddr_t dst;

	bzero ((char*)&serv_addr, sizeof (serv_addr));
	serv_addr.sin_family 	= AF_INET;
	serv_addr.sin_addr.s_addr	= inet_addr ("127.0.0.1");
    serv_addr.sin_port  = htons (2345);
    sockfd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
	connect (sockfd, (struct sockaddr*)&serv_addr, sizeof (serv_addr));

    inet_aton("127.0.0.1", (struct in_addr*)&dst);
    int a = send_tcp_packet(dst, htons(2345), htons(2345), 0, 1000, 0x80, 1000, "mess" ,4);
	close (sockfd);
	exit (0);
	return 0;	
}
