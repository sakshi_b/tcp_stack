#include <stdio.h>
#include"tcp.h"
#include<netinet/in.h>

#define IP_1 "192.168.0.1"
#define IP_2 "192.168.0.2"

int main(void) {
  int len;
  ipaddr_t dst;
  inet_aton(IP_1, (struct in_addr*)&dst);

  len = ip_send( dst, 17, 2, "hello", 6 );
  printf("len=%d\n",len);
}

