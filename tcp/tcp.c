#include<stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <tcp.h>

static long int TCB_BUF_SIZE = 1000;

typedef char u8_t;
typedef unsigned short int u16_t;
typedef unsigned int u32_t;
typedef bool state_t;

typedef enum{
    FLAG_ACK = 0x40, FLAG_SYN = 0x80, FLAG_FIN = 0x04
}flag;

typedef struct tcb_s{
    ipaddr_t tcb_our_ip_addr;
    ipaddr_t tcp_their_ip_addr;
    u16_t tcb_our_port;
    u16_t tcb_their_por;
    u32_t tcb_our_sequence_num;
    u32_t tcb_our_expected_ack;
    u32_t tcb_their_sequence_num;
    char tcb_data[TCB_BUF_SIZE];
    char *tcb_p_data;
    int tcb_data_left;
    state_t tcb_state;
}tcb_t;

static tcb_t g_tcb;

int tcp_socket(){
    bzero ((char*)&g_tcb, sizeof (g_tcb));
    g_tcb.tcb_state = false;
    return 0;
}

int tcp_listen(int port, ipaddr_t *src){
    g_tcb.tcb_our_ip_addr = *src;
    g_tcb.tcb_our_port = port;
    g_tcb.tcb_state = true;
    g_tcb.tcb_our_sequence_num = 1000;
    g_tcb.tcb_our_expexted_ack = 1;
    g_tcb.tcb_their_sequence_num = 0;
    return 0;
}

int tcp_connect(ipaddr_t dst, int port){
    g_tcb.tcb_their_ip_addr = dst;
    g_tcb.tcb_their_port = port
    g_tcb.tcb_state = true;
    g_tcb.tcb_our_sequence_num = 0;
    g_tcb.tcb_our_expexted_ack = 1000;
    g_tcb.tcb_their_sequence_num = 1000;
    return 0;
}

int tcp_read(char *buf, int maxlen){
    int r = 0, packet_size; 
    //while(sizeof(*buf) == 0)
    r = handle_packet();
    
    if (r>1){
        printf("Length of the recieved packet %d", r);
        packet_size = min(maxlen, r);
        printf(g_tcb.tcb_data);    
        return packet_size;
    }
    if(r == -1)
        return -1;
}

int tcp_write(char *buf, int len){
    int packet_size, ack;
    g_tcb.tcb_data_left = len;
    while(g_tcb.tcb_data_left>0){
    packet_size = min(g_tcb.tcb_data_left,TCB_BUF_SIZE);
    do{
        send_tcp_packet();
        ack = wait_for_packet();
    }while(ack != g_tcb.our_expected_ack);
    g_tcb.tcb_data_left-=packet_size;
    }
}

int close(){
}

int send_tcp_packet(ipaddr_t dst, u16_t src_port, u_16 dst_port, u32_t seq_nb, u32_t ack_nb, u8_t flags, u16_t win_sz, const char *data, int data_sz){
    int total_size = header_size+data_size;
    static int id = 0;
    int res = ip_send(dst, IPPROTO_TCP, ++id , , total_size);
    return res;
}

int recv_tcp_packet(ipaddr_t *src, u16_t *src_port, u_16 *dst_port, u32_t *seq_nb, u32_t *ack_nb, u8_t *flags, u16_t *win_sz, const char *data, int *data_sz){
}

static int handle_packet(){
    while(len = recv_tcp_packet()){
        if(len!=0){
            return len;
        }
        else if( fin set){
            tcp_close();
            return 0;
        }
        else if(ack contains){
            if(*ack_nb == g_tcb.our_expected_ack)
            return 1;
        }
        else return -1;
    }
}

static int wait_for_packet(){
    int ack = recv_tcp_packet();
    return ack;
}

int min (int a, int b){
    if(a<b)
        return a;
    return b;
}

