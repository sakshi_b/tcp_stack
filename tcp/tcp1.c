#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include "tcp.h"
#define TCB_BUF_SIZE 1000
#define TCP_WIN_SIZE 1
#define TCP_HEADER_LEN 5
#define TCP_PACKET_LEN 512
typedef char u8_t;
typedef unsigned short int u16_t;
typedef unsigned int u32_t;
typedef bool state_t;

typedef enum{
    FLAG_ACK = 0x40, FLAG_SYN = 0x80, FLAG_FIN = 0x04
}flag;

typedef struct {
    u16_t our_port;
    u16_t their_port;
    u32_t our_sequence_num;
    u32_t our_expected_ack;
    u8_t  header_len;
    u8_t  flags;
    u16_t win_size;
    u16_t checksum;
}tcb_ht;

typedef struct tcb_s {
	ipaddr_t 	tcb_our_ip_addr;
	ipaddr_t 	tcb_their_ip_addr;
	u16_t		tcb_our_port;
	u16_t		tcb_their_port;
	u32_t		tcb_our_sequence_num;	
	u32_t		tcb_our_expected_ack;
	u32_t		tcb_their_sequence_num;
	char		tcb_data[TCB_BUF_SIZE];
	char		*tcb_p_data;
	int		tcb_data_left;
	state_t		tcb_state;
} tcb_t;

static tcb_t g_tcb;
static tcb_ht *tcp_header;
static char *tcp_header_pkt;
static char *tcp_pkt;


int tcp_socket(){
    tcp_pkt = (char *) malloc (TCP_PACKET_LEN);

    int pkt_size = TCB_BUF_SIZE + TCP_HEADER_LEN;
    tcp_header_pkt = (char *) malloc (pkt_size);
    tcp_header = (tcb_ht *) tcp_header_pkt;
    bzero ((char*)&g_tcb, sizeof (g_tcb));
    g_tcb.tcb_state = false;
    return 0;
}

int tcp_listen(int port, ipaddr_t *src){
    g_tcb.tcb_our_ip_addr = *src;
    g_tcb.tcb_our_port = port;
    g_tcb.tcb_state = true;
    g_tcb.tcb_our_sequence_num = 1000;
    g_tcb.tcb_our_expected_ack = 1;
    g_tcb.tcb_their_sequence_num = 0;
    return 0;
}

int tcp_connect(ipaddr_t dst, int port){
    g_tcb.tcb_their_ip_addr = dst;
    g_tcb.tcb_their_port = port;
    g_tcb.tcb_state = true;
    g_tcb.tcb_our_sequence_num = 0;
    g_tcb.tcb_our_expected_ack = 1000;
    g_tcb.tcb_their_sequence_num = 1000;
    return 0;   
}

int tcp_read(char *buf, int maxlen){
    int len = 0, max_allowed_size; 
    len = handle_packet();
    
    if (len > 1){
        printf("Length of the recieved packet %d", len);
        max_allowed_size = min(maxlen, len);
        printf(g_tcb.tcb_data);
            
        return max_allowed_size;
    }
    if(len == -1)
        return -1;
}

int tcp_close(){
}

int send_tcp_packet(ipaddr_t dst, u16_t src_port, u16_t dst_port, u32_t seq_nb, u32_t ack_nb, u8_t flags, u16_t win_sz, const char *data, int data_sz){
    static unsigned short id = 0;
    int res, total_size;
    char *msg;
    tcb_ht *my_send_packet;
    total_size = data_sz + TCP_HEADER_LEN;
    msg = malloc(total_size * 4);
    my_send_packet = (tcb_ht *)msg;

    my_send_packet->our_port = src_port;
    my_send_packet->their_port = dst_port;
    my_send_packet->our_sequence_num = htonl(seq_nb);
    my_send_packet->our_expected_ack = htonl(ack_nb);
    my_send_packet->header_len = TCP_HEADER_LEN;
    my_send_packet->flags = flags;
    my_send_packet->win_size = htons(TCP_WIN_SIZE);
    my_send_packet->checksum = 0xFFFF;
    //copy the data to the remaining memory
    memcpy( ( msg + TCP_HEADER_LEN ), data, data_sz );
    msg[total_size] = '\0'; 
    res = ip_send(dst, 17, ++id , msg , total_size);
    free(msg);
    return res;
}

//int recv_tcp_packet(ipaddr_t *src, u16_t *src_port, u16_t *dst_port, u32_t *seq_nb, u32_t *ack_nb, u8_t *flags, u16_t *win_sz, const char *data, int *data_sz){
int recv_tcp_packet( ipaddr_t *src_addr, u16_t *src_port, u16_t *dst_port, char **data ){
    int len;
    //int header_len, data_len;
    ipaddr_t srcp, dstp;
    unsigned short proto, id;
    //tcb_ht *my_recv_packet;
    char *msg;
    len = ip_receive(&srcp, &dstp, &proto, &id, &msg);
    printf("Packet length:%d ", len); 
    //data_len = len - TCP_HEADER_LEN;
    //my_recv_packet = (tcb_ht *)msg;
    
    //copy data from the remaining memory
    return len;
}

int tcp_write(char *buf, int len){
    int packet_size, ack;
    strcpy(g_tcb.tcb_data, buf);
    g_tcb.tcb_data_left = len;
    while(g_tcb.tcb_data_left>0){
    packet_size = min(g_tcb.tcb_data_left,TCB_BUF_SIZE);
    //do{
        send_tcp_packet( g_tcb.tcb_their_ip_addr, g_tcb.tcb_our_port, g_tcb.tcb_their_port, g_tcb.tcb_our_sequence_num, g_tcb.tcb_our_expected_ack, FLAG_SYN, TCP_WIN_SIZE, g_tcb.tcb_data, packet_size );

    //    ack = wait_for_packet();
    //}while(ack != g_tcb.tcb_our_expected_ack);
    g_tcb.tcb_our_expected_ack += 1;
    g_tcb.tcb_data_left-=packet_size;
    }
//    return packet_size;
return packet_size;
}

int handle_packet(){
    int len;
    //while(len = recv_tcp_packet(&g_tcb.tcb_their_ip_addr, &g_tcb.tcb_their_port, &g_tcb.tcb_our_port, &g_tcb.tcb_their_sequence_num, &g_tcb.tcb_our_expected_ack, &tcp_header->flags, &tcp_header->win_size, &g_tcb.tcb_data, &len)){
      while(len = recv_tcp_packet(&g_tcb.tcb_their_ip_addr, &g_tcb.tcb_their_port, g_tcb.tcb_our_port, &tcp_pkt) ){
          if(len!=0){
            return len;
        }
        else if(tcp_header->flags == FLAG_FIN){
            tcp_close();
            return 0;
        }
        else if(tcp_header->flags == FLAG_ACK){
            if(g_tcb.tcb_their_sequence_num == g_tcb.tcb_our_expected_ack){
                g_tcb.tcb_our_expected_ack += 1;
                printf("Ack recieved: %d", g_tcb.tcb_their_sequence_num);
                return 0;
            }
            else return -1;
        }
        else return -1;
    }
}

int wait_for_packet(){
    int len;
    //len = recv_tcp_packet(&g_tcb.tcb_their_ip_addr, &g_tcb.tcb_their_port, &g_tcb.tcb_our_port, &g_tcb.tcb_their_sequence_num, &g_tcb.tcb_our_expected_ack, &tcp_header->flags, &tcp_header->win_size, &g_tcb.tcb_data, &len);
    len = recv_tcp_packet(&g_tcb.tcb_their_ip_addr, &g_tcb.tcb_their_port, g_tcb.tcb_our_port, &tcp_pkt);

    if(tcp_header->flags == FLAG_ACK){
        if(g_tcb.tcb_their_sequence_num == g_tcb.tcb_our_expected_ack){
            g_tcb.tcb_our_expected_ack += 1;
            printf("Ack recieved: %d", g_tcb.tcb_their_sequence_num);
            return g_tcb.tcb_their_sequence_num;
            }
            else return -1;
    }
}

int min (int a, int b){
    if(a<b)
        return a;
    return b;
}

