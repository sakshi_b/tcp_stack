#include<sys/types.h>
#include<sys/socket.h>
#include<string.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>

typedef unsigned long ipaddr_t;
typedef unsigned short int u16_t;

int main()
{
	struct sockaddr_in serv_addr, client_addr;
    struct sockaddr_in sa;
	int sockfd, newsockfd, clientlen = sizeof (client_addr), childpid, i, len;

	sockfd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    bzero ((char*)&serv_addr, sizeof (serv_addr));
	serv_addr.sin_family 		= AF_INET;
	serv_addr.sin_addr.s_addr 	= inet_addr ("127.0.0.1");
	serv_addr.sin_port		= htons (2345);
	bind (sockfd,(struct sockaddr*) &serv_addr, sizeof (serv_addr));

    char *msg;
    ipaddr_t src;
    u16_t port;

	listen (sockfd, 5);
	for (;;)
	{
		clientlen = sizeof (client_addr);
		newsockfd = accept (sockfd, (struct sockaddr*)&client_addr, &clientlen);
		if ((childpid = fork ()) == 0)
		{
			close (sockfd);
            len = recv_tcp_packet(&src, &port, htons(2345), 1, 1, 0x00, 1000, &msg,10);
			printf("%d",len);
            exit (0); //free the resourses
		}
		close (newsockfd);
	}
	return 0;
}

