/*
 * sockSend.c
 *
 *  Created on: 23-Apr-2011
 *      Author: Rahul Krishnan
 */

#include <stdio.h>
#include"tcp.h"
#include<netinet/in.h>

#define THEIR_PORT 4444

#define OUR_IP "192.168.0.2"
#define THEIR_IP "192.168.0.1"

int main() {
	ipaddr_t dst;
	u16_t dst_p;
	dst_p = htons( THEIR_PORT );
	inet_aton( THEIR_IP, ( struct in_addr * ) &dst );

	tcp_socket();
	tcp_connect( dst, dst_p );
	tcp_close();
}


